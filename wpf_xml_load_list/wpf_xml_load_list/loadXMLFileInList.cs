﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq; 

namespace wpf_xml_load_list
{
	public class Order
	{
		public int orderID { get; set; }
		public DateTime orderDate { get; set; }
		public decimal total { get; set; }

	}


	public class Customer
	{
		public string customerID { get; set; }
		public string companyName{ get; set; }
		public string address { get; set; }
		public string city { get; set; }
		public string region { get; set; }
		public string postalCode{ get; set; }
		public string country { get; set; }
		public string phone { get; set; }
		public string fax { get; set; }
		public Order[] orders { get; set; }

	}

	class Program
	{
		public static List<Customer> customerList;

		static void Main(string[] args)
		{
			customerList =
				(
					from e XDocument.Load("C:\\Users\\Adriana\\Documents\\wpf_xml_load_list\\wpf_xml_load_list\\wpf_xml_load_list\\bin\\Debug");
			Root.Elements("customers");
				)
		}
	}

}
